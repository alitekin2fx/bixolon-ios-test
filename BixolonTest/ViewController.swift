//
//  ViewController.swift
//  BixolonTest
//
//  Created by ali teke on 23/10/2018.
//  Copyright © 2018 ali teke. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var printer: BixolonPrinter!
    @IBOutlet weak var logView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        printer = BixolonPrinter(logView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func testCpclTapped(_ sender: Any) {
        printer.testCpcl()
    }
    
    @IBAction func testBarcodeTapped(_ sender: Any) {
        printer.testBarcode()
    }
}

